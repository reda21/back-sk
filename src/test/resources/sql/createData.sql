/*
-- Query: SELECT * FROM sakila.category
LIMIT 0, 1000

-- Date: 2Feb1-01-03 22:16
*/
INSERT INTO public.t_category (id, create_date, last_update, name) VALUES (1,TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),'Action');
INSERT INTO public.t_category (id, create_date, last_update, name) VALUES (2,TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),'Animation');
INSERT INTO public.t_category (id, create_date, last_update, name) VALUES (3,TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),'Children');
INSERT INTO public.t_category (id, create_date, last_update, name) VALUES (4,TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),'Classics');
INSERT INTO public.t_category (id, create_date, last_update, name) VALUES (5,TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),'Comedy');
INSERT INTO public.t_category (id, create_date, last_update, name) VALUES (6,TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),'Documentary');
INSERT INTO public.t_category (id, create_date, last_update, name) VALUES (7,TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),'Drama');
INSERT INTO public.t_category (id, create_date, last_update, name) VALUES (8,TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),'Family');
INSERT INTO public.t_category (id, create_date, last_update, name) VALUES (9,TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),'Foreign');
INSERT INTO public.t_category (id, create_date, last_update, name) VALUES (10,TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),'Games');
INSERT INTO public.t_category (id, create_date, last_update, name) VALUES (11,TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),'Horror');
INSERT INTO public.t_category (id, create_date, last_update, name) VALUES (12,TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),'Music');
INSERT INTO public.t_category (id, create_date, last_update, name) VALUES (13,TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),'New');
INSERT INTO public.t_category (id, create_date, last_update, name) VALUES (14,TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),'Sci-Fi');
INSERT INTO public.t_category (id, create_date, last_update, name) VALUES (15,TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),'Sports');
INSERT INTO public.t_category (id, create_date, last_update, name) VALUES (16,TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),'Travel');

INSERT INTO public.t_language (id,create_date,last_update,name) VALUES (1,TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),'English');
INSERT INTO public.t_language (id,create_date,last_update,name) VALUES (2,TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),'Italian');
INSERT INTO public.t_language (id,create_date,last_update,name) VALUES (3,TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),'Japanese');
INSERT INTO public.t_language (id,create_date,last_update,name) VALUES (4,TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),'Mandarin');
INSERT INTO public.t_language (id,create_date,last_update,name) VALUES (5,TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),'French');
INSERT INTO public.t_language (id,create_date,last_update,name) VALUES (6,TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),'German');



INSERT INTO public.t_film (id, create_date,last_update,description,picture,length,original_language_id,rating,release_year,rental_duration,rental_rate,replacement_cost, special_features,title,category_id,language_id) VALUES (1,TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),TO_TIMESTAMP('2006-02-15 05:03:43', 'YYYY-MM-DD HH24:MI:SS'),'A Epic Drama of a Feminist And a Mad Scientist who must Battle a Teacher in The Canadian Rockies',NULL,86,1,'PG13',2006,6,0.99,20.99,'Deleted Scenes,Behind the Scenes','ACADEMY DINOSAUR',1,1);
