/**
 * 
 */
package com.custom.backend.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import com.custom.backend.dto.FilmDto;
import com.custom.backend.mapper.FilmMapper;
import com.custom.backend.model.Film;
import com.custom.backend.persistence.interf.FilmRepository;

/**
 * @author redinho20
 *
 */
class FilmServiceSakilaImplTest {
		
	@InjectMocks
	private FilmServiceSakilaImpl filmService = new FilmServiceSakilaImpl();
	
	@Mock
	private FilmRepository filmRepository;
	
	@Mock
	private FilmMapper filmMapper;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterEach
	void tearDown() throws Exception {
	}


	/**
	 * Test method for {@link com.custom.backend.service.impl.FilmServiceSakilaImpl#getFilms()}.
	 */
	@DisplayName("Test getFilms service")
	@Test
	void testGetFilms() {
		Film film = new Film();
		Mockito.when(filmRepository.findAll()).thenReturn(Arrays.asList(film));
		Mockito.when(filmMapper.entityToDto(film)).thenReturn(new FilmDto());
		
		assertEquals(1, filmService.getFilms().size());
	}
}
