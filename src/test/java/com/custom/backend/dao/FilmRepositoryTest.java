/**
 * 
 */
package com.custom.backend.dao;


import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.jdbc.Sql;

import com.custom.backend.model.Film;
import com.custom.backend.persistence.interf.FilmRepository;


/**
 * @author Redinho16
 *
 */

// On configure JUnit pour utiliser directement Spring
// On pointe vers le fichier de configuration Spring à la racine du classpath
@DataJpaTest
@PropertySource(value = { "classpath:application.yml" })
public class FilmRepositoryTest  {

    @Autowired
	private FilmRepository filmRepository;

	@BeforeEach
	public void setUp() {
	}

	@Test
    @Sql("classpath:sql/createData.sql")
	public void testGetFilms() {
			 List<Film> films = filmRepository.findAll();
			 Assertions.assertNotNull(films);
	}
}
