package com.custom.backend.dao;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.jdbc.Sql;

import com.custom.backend.model.Category;
import com.custom.backend.persistence.interf.CategoryRepository;

@DataJpaTest
@PropertySource(value = { "classpath:application.yml" })
public class CategoryRepositoryTest {

    @Autowired
    private CategoryRepository categoryRepository;

    @BeforeEach
    public void setUp() {
    }

    @Test
    @Sql("classpath:sql/createData.sql")
    public void testGetCategories() throws Exception {

        List<Category> categories = categoryRepository.findAll();
        Assertions.assertNotNull(categories);

    }
}
