/*
-- Query: SELECT * FROM sakila.language
LIMIT 0, 1000

-- Date: 2021-01-03 22:14
*/
INSERT INTO public.t_language (id,create_date,last_update,name) VALUES (1,'2006-02-15 05:02:19','2006-02-15 05:02:19','English');
INSERT INTO public.t_language (id,create_date,last_update,name) VALUES (2,'2006-02-15 05:02:19','2006-02-15 05:02:19','Italian');
INSERT INTO public.t_language (id,create_date,last_update,name) VALUES (3,'2006-02-15 05:02:19','2006-02-15 05:02:19','Japanese');
INSERT INTO public.t_language (id,create_date,last_update,name) VALUES (4,'2006-02-15 05:02:19','2006-02-15 05:02:19','Mandarin');
INSERT INTO public.t_language (id,create_date,last_update,name) VALUES (5,'2006-02-15 05:02:19','2006-02-15 05:02:19','French');
INSERT INTO public.t_language (id,create_date,last_update,name) VALUES (6,'2006-02-15 05:02:19','2006-02-15 05:02:19','German');
