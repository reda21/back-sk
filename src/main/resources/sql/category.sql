/*
-- Query: SELECT * FROM sakila.category
LIMIT 0, 1000

-- Date: 2021-01-03 22:16
*/
INSERT INTO public.t_category (id, create_date, last_update, name) VALUES (1,'2006-02-15 04:46:27','2006-02-15 04:46:27','Action');
INSERT INTO public.t_category (id, create_date, last_update, name) VALUES (2,'2006-02-15 04:46:27','2006-02-15 04:46:27','Animation');
INSERT INTO public.t_category (id, create_date, last_update, name) VALUES (3,'2006-02-15 04:46:27','2006-02-15 04:46:27','Children');
INSERT INTO public.t_category (id, create_date, last_update, name) VALUES (4,'2006-02-15 04:46:27','2006-02-15 04:46:27','Classics');
INSERT INTO public.t_category (id, create_date, last_update, name) VALUES (5,'2006-02-15 04:46:27','2006-02-15 04:46:27','Comedy');
INSERT INTO public.t_category (id, create_date, last_update, name) VALUES (6,'2006-02-15 04:46:27','2006-02-15 04:46:27','Documentary');
INSERT INTO public.t_category (id, create_date, last_update, name) VALUES (7,'2006-02-15 04:46:27','2006-02-15 04:46:27','Drama');
INSERT INTO public.t_category (id, create_date, last_update, name) VALUES (8,'2006-02-15 04:46:27','2006-02-15 04:46:27','Family');
INSERT INTO public.t_category (id, create_date, last_update, name) VALUES (9,'2006-02-15 04:46:27','2006-02-15 04:46:27','Foreign');
INSERT INTO public.t_category (id, create_date, last_update, name) VALUES (10,'2006-02-15 04:46:27','2006-02-15 04:46:27','Games');
INSERT INTO public.t_category (id, create_date, last_update, name) VALUES (11,'2006-02-15 04:46:27','2006-02-15 04:46:27','Horror');
INSERT INTO public.t_category (id, create_date, last_update, name) VALUES (12,'2006-02-15 04:46:27','2006-02-15 04:46:27','Music');
INSERT INTO public.t_category (id, create_date, last_update, name) VALUES (13,'2006-02-15 04:46:27','2006-02-15 04:46:27','New');
INSERT INTO public.t_category (id, create_date, last_update, name) VALUES (14,'2006-02-15 04:46:27','2006-02-15 04:46:27','Sci-Fi');
INSERT INTO public.t_category (id, create_date, last_update, name) VALUES (15,'2006-02-15 04:46:27','2006-02-15 04:46:27','Sports');
INSERT INTO public.t_category (id, create_date, last_update, name) VALUES (16,'2006-02-15 04:46:27','2006-02-15 04:46:27','Travel');
