package com.custom.backend.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.custom.backend.dto.CountryDto;
import com.custom.backend.mapper.CountryMapper;
import com.custom.backend.persistence.interf.CountryRepository;
import com.custom.backend.model.Country;
import com.custom.backend.service.interf.CountryServiceSakila;

@Service("countryServiceSakila")
public class CountryServiceSakilaImpl implements CountryServiceSakila {

    @Autowired
    private CountryRepository countryRepository;

    private CountryMapper countryMapper;

    @PostConstruct
    public void mapStructMapper() {
        countryMapper = Mappers.getMapper(CountryMapper.class);
    }

    @Override
    @Transactional
    public List<CountryDto> getCountries() {

        List<Country> countries = countryRepository.findAll();
        if(!CollectionUtils.isEmpty(countries)) {
            return countries.stream().map(entity -> countryMapper.mapEntityToDto(entity)).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }
}
