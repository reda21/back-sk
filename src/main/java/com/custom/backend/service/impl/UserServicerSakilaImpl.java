/**
 *
 */
package com.custom.backend.service.impl;

import javax.annotation.PostConstruct;

import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.custom.backend.dto.UserDto;
import com.custom.backend.mapper.UserMapper;

import com.custom.backend.persistence.interf.UserRepository;
import com.custom.backend.model.User;
import com.custom.backend.service.interf.UserServiceSakila;

/**
 * @author Redinho16
 *
 */
@Service("userServiceSakila")
public class UserServicerSakilaImpl implements UserServiceSakila {

    @Autowired
    private UserRepository userRepository;

    private UserMapper userMapper;

    @PostConstruct
    public void mapStructMapper() {
        userMapper = Mappers.getMapper(UserMapper.class);
    }


    @Override
    public UserDto saveUser(User user) {
        return userMapper.userEntityToUserDto(userRepository.save(user));
    }


	@Override
	public UserDto getUser(String name) {
		 return userMapper.userEntityToUserDto(userRepository.findByUserName(name));
	}
}
