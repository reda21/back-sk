package com.custom.backend.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.custom.backend.dto.FilmDto;
import com.custom.backend.mapper.FilmMapper;
import com.custom.backend.persistence.interf.FilmRepository;
import com.custom.backend.model.Film;
import com.custom.backend.service.interf.FilmServiceSakila;

@Service("filmServiceSakila")
public class FilmServiceSakilaImpl implements FilmServiceSakila {

    @Autowired
    private FilmRepository filmRepository;

    private FilmMapper filmMapper;

    @PostConstruct
    public void mapStructMapper() {
        filmMapper = Mappers.getMapper(FilmMapper.class);
    }


    @Override
    @Transactional
    public List<FilmDto> getFilms() {

        List<Film> films = filmRepository.findAll();

        if(!CollectionUtils.isEmpty(films)) {
            return films.stream().map(entity -> filmMapper.entityToDto(entity)).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }
}
