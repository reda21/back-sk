package com.custom.backend.service.impl;

import com.custom.backend.dto.CategoryDto;
import com.custom.backend.mapper.CategoryMapper;
import com.custom.backend.persistence.interf.CategoryRepository;
import com.custom.backend.model.Category;
import com.custom.backend.service.interf.CategoryServiceSakila;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Configuration
@Service("categoryServiceSakila")
@EntityScan("com.custom.backend.model")
@EnableJpaRepositories("com.custom.backend.persistence.interf")
//@org.springframework.boot.autoconfigure.EnableAutoConfiguration
//@EnableJpaRepositories(includeFilters = @Filter(value = {org.springframework.stereotype.Repository.class},
//type = FilterType.ANNOTATION))
public class CategoryServiceSakilaImpl implements CategoryServiceSakila {

    @Autowired
    private CategoryRepository categoryRepository;

    private CategoryMapper categoryMapper;

    @PostConstruct
    public void mapStructMapper() {
        categoryMapper = Mappers.getMapper(CategoryMapper.class);
    }

    @Transactional
    @Override
    public List<CategoryDto> findAll() {

        List<Category> categories = categoryRepository.findAll();

        if(!CollectionUtils.isEmpty(categories)) {
            return categories.stream().map(entity -> categoryMapper.EntityToDto(entity)).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }
}
