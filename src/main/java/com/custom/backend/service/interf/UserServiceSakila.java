package com.custom.backend.service.interf;

import com.custom.backend.dto.UserDto;
import com.custom.backend.model.User;



public interface UserServiceSakila {

    public UserDto getUser(String name);
    public UserDto saveUser(User user);

}
