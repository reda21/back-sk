package com.custom.backend.service.interf;

import com.custom.backend.dto.CategoryDto;
import com.custom.backend.model.Category;
import java.util.List;

public interface CategoryServiceSakila {

    public List<CategoryDto> findAll();
}
