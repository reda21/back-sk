package com.custom.backend.service.interf;

import com.custom.backend.dto.CustomerDto;
import com.custom.backend.model.Custom;

public interface CustomerServiceSakila {

    public CustomerDto getCustomer(String name);
    public CustomerDto saveCustomer(Custom custom);

}
