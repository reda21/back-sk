package com.custom.backend.service.interf;

import com.custom.backend.dto.CountryDto;

import java.util.List;

public interface CountryServiceSakila {

    public List<CountryDto> getCountries();
}
