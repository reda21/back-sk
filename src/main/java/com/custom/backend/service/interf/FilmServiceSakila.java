package com.custom.backend.service.interf;


import com.custom.backend.dto.FilmDto;
import com.custom.backend.model.Film;

import java.util.List;

public interface FilmServiceSakila {

    public List<FilmDto> getFilms();

}
