package com.custom.backend.mapper;

import com.custom.backend.dto.CategoryDto;
import com.custom.backend.model.Category;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface CategoryMapper {

    @Mapping(target = "filmCategories", ignore = true)
    CategoryDto EntityToDto(Category category);

}
