package com.custom.backend.mapper;

import com.custom.backend.dto.InventoryDto;
import com.custom.backend.model.Inventory;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

public interface InventoryMapper {


    //InventoryDto EntityToDto(Inventory inventory);

    @Named("inventoryMappingWithoutFilm")
    @Mapping(target ="film", ignore = true)
    InventoryDto EntityToDtoIgnoreFilm(Inventory inventory);
}
