package com.custom.backend.mapper;

import com.custom.backend.dto.UserDto;
import com.custom.backend.model.User;

import org.mapstruct.Mapper;

@Mapper
public interface UserMapper {


    public UserDto userEntityToUserDto(User user);
}
