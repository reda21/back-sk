package com.custom.backend.mapper;

import com.custom.backend.dto.FilmDto;
import com.custom.backend.model.Film;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(uses= {ActorMapper.class, LanguageMapper.class, RatingMapping.class, InventoryMapper.class})
public interface FilmMapper {

    @IterableMapping(qualifiedByName = {"languageMappingWithoutFilm", "actorMappingWithoutFilm", "inventoryMappingWithoutFilm"})
    @Mapping(target = "category.filmCategories", ignore = true)
    @Mapping(source = "language", target ="language", qualifiedByName="inventoryMappingWithoutFilm")
    FilmDto entityToDto(Film film);
}
