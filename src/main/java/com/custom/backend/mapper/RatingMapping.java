package com.custom.backend.mapper;

import com.custom.backend.model.Rating;
import org.mapstruct.MapperConfig;


@MapperConfig
public class RatingMapping {

    protected String mapEnum(Rating rating) {
        if(rating == null) return null;
        return rating.toString();
    }

    //abstract RatingSource toSource(RatingDto target);

    //abstract RatingDto totarget(RatingSource source);

    //String toString(Rating test){
    //  return test.name();
    //}

    //Rating toEnum(String code){
    //  for (Rating rating : Rating.values()) {
    //    if(rating.equals(code)){
    //      return rating;
    // }
    // }
    // return null;
    // }
}
