package com.custom.backend.mapper;

import com.custom.backend.dto.CountryDto;
import com.custom.backend.model.Country;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(uses= CityMapper.class)
public interface CountryMapper {

    @Mapping(source = "cities", target ="cities", qualifiedByName="mappingWithoutCountry")
    @Mapping(target ="lastUpdate", ignore = true) @Mapping(target ="createDate", ignore = true)
    CountryDto mapEntityToDto(Country country);
}
