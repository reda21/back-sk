package com.custom.backend.mapper;

import com.custom.backend.dto.CityDto;
import com.custom.backend.model.City;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper
public interface CityMapper {

    @Named("mappingWithoutCountry")
    @Mapping(target ="country", ignore = true)
    @Mapping(target ="lastUpdate", ignore = true) @Mapping(target ="createDate", ignore = true)
    CityDto EntityToDtoIgnoreCountry(City city);
}
