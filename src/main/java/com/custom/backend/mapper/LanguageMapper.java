package com.custom.backend.mapper;

import com.custom.backend.dto.LanguageDto;
import com.custom.backend.model.Language;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper
public interface LanguageMapper {

    @Named("languageMappingWithoutFilms")
    @Mapping(target ="lastUpdate", ignore = true) @Mapping(target ="createDate", ignore = true)
    @Mapping(target = "films", ignore = true)
    LanguageDto EntityToDtoIgnoreFilm(Language language);
}
