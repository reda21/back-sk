package com.custom.backend.mapper;

import com.custom.backend.dto.ActorDto;
import com.custom.backend.model.Actor;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper(uses = FilmMapper.class)
public interface ActorMapper {

    @Named("actorMappingWithoutFilm")
    @Mapping(target="films", ignore = true)
    ActorDto EntityToDtoIgnoreFilm(Actor actor);
}
