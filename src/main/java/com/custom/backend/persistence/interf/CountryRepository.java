package com.custom.backend.persistence.interf;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.custom.backend.model.Country;

public interface CountryRepository extends JpaSpecificationExecutor<Country>, JpaRepository<Country, Integer> {
}
