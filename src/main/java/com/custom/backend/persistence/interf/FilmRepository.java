/**
 * 
 */
package com.custom.backend.persistence.interf;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.custom.backend.model.Film;



/**
 * @author Redinho16
 *
 */
public interface FilmRepository extends JpaSpecificationExecutor<Film>, JpaRepository<Film, Integer> {



}
