/**
 * 
 */
package com.custom.backend.persistence.interf;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.custom.backend.model.User;




/**
 * @author Redinho16
 *
 */
public interface UserRepository extends JpaSpecificationExecutor<User>, JpaRepository<User, Integer>  {

    public User findByUserName(String userName);

}
