package com.custom.backend.persistence.interf;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.custom.backend.model.Category;

public interface CategoryRepository extends JpaSpecificationExecutor<Category>, JpaRepository<Category, Integer> {
}
