/**
 * 
 */

package com.custom.backend.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;

/**
 * @author Redinho16
 *
 */
@Entity
@javax.persistence.Table(name = "T_PAYMENT")
public class Payment extends CommonClass implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1764517377793334051L;

	@ManyToOne(fetch = FetchType.LAZY)
	private Custom customer;

	@ManyToOne(fetch = FetchType.LAZY)
	private Staff staff;

	@OneToOne
	@MapsId
	@JoinColumn(name = "id")
	private Rental rental;

	@Column(name = "amount", nullable = false, length = 5, scale = 2)
	private Double amount;

	@Column(name = "payment_date", nullable = false)
	private LocalDate payment_date;

	public Custom getCustomer() {
		return customer;
	}

	public void setCustomer(Custom customer) {
		this.customer = customer;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public Rental getRental() {
		return rental;
	}

	public void setRental(Rental rental) {
		this.rental = rental;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public LocalDate getPayment_date() {
		return payment_date;
	}

	public void setPayment_date(LocalDate payment_date) {
		this.payment_date = payment_date;
	}



}
