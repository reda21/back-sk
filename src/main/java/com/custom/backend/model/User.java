/**
 *
 */

package com.custom.backend.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;


/**
 * @author Redinho16
 *
 */
@Entity
@javax.persistence.Table(name = "T_USER")
public class User extends CommonClass implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -5106313775838603382L;

	@Column(name = "user_name")
	private String userName;

	@Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    @Column(name = "roles")
    private String roles;

}
