
/**
 * 
 */
package com.custom.backend.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import org.hibernate.annotations.UpdateTimestamp;

/**
 * @author Redinho16
 *
 */
@Entity
@javax.persistence.Table(name = "T_ACTOR")
public class Actor extends CommonClass implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6750203184792654128L;

	@Column(name = "first_name", length = 45, nullable = false)
	private String firstName;

	@Column(name = "last_name", length = 45, nullable = false)
	private String lastName;

	//@ManyToMany(mappedBy = "actors")
	//private Set<Film> films = new HashSet<>();


	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/*public Set<Film> getFilms() {
		return films;
	}

	public void setFilms(Set<Film> films) {
		this.films = films;
	}
*/
}


