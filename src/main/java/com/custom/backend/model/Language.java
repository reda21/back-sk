/**
 * 
 */
package com.custom.backend.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

/**
 * @author Redinho16
 *
 */
@Entity
@javax.persistence.Table(name = "T_LANGUAGE")
public class Language extends CommonClass implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3858331670925092085L;

	@OneToMany(mappedBy = "language", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<Film> films = new HashSet<Film>();

	@Column(name = "name", nullable = false, length = 20)
	private String name;

	public Set<Film> getFilms() {
		return films;
	}

	public void setFilms(Set<Film> films) {
		this.films = films;
	}


	public String getName() {
		return name;
	}

	public void setName(String pName) {
		name = pName;
	}
}
