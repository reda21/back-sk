/**
 * 
 */

package com.custom.backend.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.CreationTimestamp;

/**
 * @author Redinho16
 *
 */

@Entity
@javax.persistence.Table(name = "T_STORE")
public class Store extends CommonClass implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7523272381860865978L;

	@OneToOne(mappedBy = "store")
	private Staff staff;

	@OneToMany(mappedBy = "store", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<Custom> customers = new HashSet<>();

	@OneToMany(mappedBy = "store", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<Inventory> inventories = new HashSet<Inventory>();

	public Set<Custom> getCustomers() {
		return customers;
	}

	public void setCustomers(Set<Custom> customers) {
		this.customers = customers;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff pStaff) {
		staff = pStaff;
	}


	/**
	 * @return the inventories
	 */

	public Set<Inventory> getInventories() {
		return inventories;
	}

	/**
	 * @param pInventories the inventories to set
	 */
	public void setInventories(Set<Inventory> pInventories) {
		inventories = pInventories;
	}

}
