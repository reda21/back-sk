/**
 * 
 */
package com.custom.backend.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;

import javax.persistence.*;

/**
 * @author Redinho16
 *
 */
@Entity
@javax.persistence.Table(name = "T_ADDRESS")
public class Address extends CommonClass implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1525664211322321171L;

	@Column(name = "address1", length = 50, nullable = false)
	private String address1;

	@Column(name = "address2", length = 50, nullable = true)
	private String address2;

	@Column(name = "district", length = 20, nullable = false)
	private String district;

	@OneToOne
	@MapsId
	@JoinColumn(name = "id")
	private City city;

	@Column(name = "postal_code", nullable = true)
	private String postalCode;

	@Column(name = "phone", nullable = false, length = 20)
	private String phoneNumber;

	@Column(name = "location", nullable = false)
	private double location;

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public City getCityId() {
		return city;
	}

	public void setCityId(City city) {
		this.city = city;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}


	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public double getLocation() {
		return location;
	}

	public void setLocation(double location) {
		this.location = location;
	}

}
