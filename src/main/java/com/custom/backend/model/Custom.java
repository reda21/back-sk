package com.custom.backend.model;

/**
 * 
 */
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * @author Redinho16
 *
 */

@Entity
@javax.persistence.Table(name="T_CUSTOMER")
public class Custom extends CommonClass implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = -6085422165167836316L;

		@Column(name = "first_name", length = 45, nullable = false)
		private String firstName;

		@Column(name = "last_name", nullable = false, length = 45)
		private String lastName;

		@Column(name = "email", nullable = true, length = 50)
		private String email;

		@OneToOne
		@MapsId
		private Address address;

		@Column(name = "active", length = 1, nullable = false)
		private boolean active;

		@OneToMany(mappedBy = "customer", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
		private Set<Rental> rentals = new HashSet<Rental>();


		@OneToMany(mappedBy = "customer", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
		private Set<Payment> payments = new HashSet<>();


		@ManyToOne(fetch = FetchType.LAZY)
		private Store store;



		/**
		 * @return the firstName
		 */
		public String getFirstName() {
			return firstName;
		}

		/**
		 * @param pFirstName the firstName to set
		 */
		public void setFirstName(String pFirstName) {
			firstName = pFirstName;
		}

		/**
		 * @return the lastName
		 */


		public String getLastName() {
			return lastName;
		}

		/**
		 * @param pLastName the lastName to set
		 */

		public void setLastName(String pLastName) {
			lastName = pLastName;
		}

		/**
		 * @return the email
		 */


		public String getEmail() {
			return email;
		}

		/**
		 * @param pEmail the email to set
		 */

		public void setEmail(String pEmail) {
			email = pEmail;
		}

		/**
		 * @return the adress
		 */

	  	//@OneToOne
	  	//@JoinColumn(name="address", nullable=false)
	  	//public Address getAddress() { return address; }

		/**
		 * @param pAddress the address to set
		 */

		//public void setAddress(Address pAddress) {
		//	address = pAddress;
		//}

		/**
		 * @return the active
		 */
		public boolean isActive() {
			return active;
		}

		/**
		 * @param pActive the active to set
		 */

		public void setActive(boolean pActive) {
			active = pActive;
		}


		/**
		 * @return the rentals
		 */

		public Set<Rental> getRentals() {
			return rentals;
		}

		/**
		 * @param pRentals the rentals to set
		 */
		public void setRentals(Set<Rental> pRentals) {
			rentals = pRentals;
		}


		public Set<Payment> getPayments() {
			return payments;
		}

		public void setPayments(Set<Payment> payments) {
			this.payments = payments;
		}

		public Store getStore() {
			return store;
		}

		public void setStore(Store store) {
			this.store = store;
		}

}
