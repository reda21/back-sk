/**
 * 
 */

package com.custom.backend.model;

import java.io.Serializable;
import java.time.Year;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.custom.backend.service.impl.YearAttributeConverter;

/**
 * @author Redinho16
 *
 */

@Entity
@javax.persistence.Table(name = "T_FILM")
public class Film extends CommonClass implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -882514394633918760L;

	@Column(name = "title", length = 255, nullable = false)
	private String title;

	@Column(name = "description", nullable = true)
	private String description;

	@Column(name = "release_year", columnDefinition = "smallint", nullable = true, length = 4)
	@Convert(
			converter = YearAttributeConverter.class
	)
	private Year releaseYear;

	@ManyToOne(fetch = FetchType.LAZY)
	private Language language;

	@Column(name = "original_language_id", nullable = false)
	private Integer originalLanguageId;

	@Column(name = "rental_duration", nullable = false)
	private Integer rentalDuration = 3;

	@Column(name = "rental_rate", scale = 2, length = 4, nullable = false)
	private Double rentalRate = 4.99;

	@Column(name = "length", length = 5, nullable = true)
	private Integer length;

	@Column(name = "replacement_cost", length = 5, scale = 2, nullable = false)
	private Double replacementCost = 19.99;

	@Column(name = "rating", nullable = true, length = 15)
	@Enumerated(EnumType.STRING)
	private Rating rating;

	@Column(name = "special_features", nullable = true)
	private String specialFeatures;

	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinTable(name = "T_FILM_ACTOR", joinColumns = @JoinColumn(name = "film_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "actor_id", referencedColumnName = "id"))
	private Set<Actor> actors = new HashSet<>();

	@ManyToOne(fetch = FetchType.LAZY)
	private Category category;

	@OneToMany(mappedBy = "film", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<Inventory> inventory = new HashSet<>();

	@Column(name = "picture", nullable = true, columnDefinition = "BLOB")
	@Lob
	private byte[] image;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	
	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	/*public Enum<Rating> getRating() {
		return rating;
	}

	public void setRating(Enum<Rating> rating) {
		this.rating = rating;
	}*/

	

	public Set<Actor> getActors() {
		return actors;
	}

	public void setActors(Set<Actor> actors) {
		this.actors = actors;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Set<Inventory> getInventory() {
		return inventory;
	}

	public void setInventory(Set<Inventory> inventory) {
		this.inventory = inventory;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public Year getReleaseYear() {
		return releaseYear;
	}

	public void setReleaseYear(Year releaseYear) {
		this.releaseYear = releaseYear;
	}

	public Integer getOriginalLanguageId() {
		return originalLanguageId;
	}

	public void setOriginalLanguageId(Integer originalLanguageId) {
		this.originalLanguageId = originalLanguageId;
	}

	public Integer getRentalDuration() {
		return rentalDuration;
	}

	public void setRentalDuration(Integer rentalDuration) {
		this.rentalDuration = rentalDuration;
	}

	public Double getRentalRate() {
		return rentalRate;
	}

	public void setRentalRate(Double rentalRate) {
		this.rentalRate = rentalRate;
	}

	public Double getReplacementCost() {
		return replacementCost;
	}

	public void setReplacementCost(Double replacementCost) {
		this.replacementCost = replacementCost;
	}

	public String getSpecialFeatures() {
		return specialFeatures;
	}

	public void setSpecialFeatures(String specialFeatures) {
		this.specialFeatures = specialFeatures;
	}

}
