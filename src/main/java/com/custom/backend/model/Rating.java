/**
 * 
 */
package com.custom.backend.model;

/**
 * @author Redinho16
 *
 */
public enum Rating {
	
	G("G"), P("P"), PG13("PG-13"), R("R"), NC17("NC-17");

	//private Rating(String s) {
	//	Rating.valueOf(s);
	//}

	private String value;

	private Rating(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
