/**
 * 
 */
package com.custom.backend.model;

import java.io.Serializable;


import javax.persistence.*;


/**
 * @author Redinho16
 *
 */
@Entity
@javax.persistence.Table(name="T_CITY")
public class City extends CommonClass implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2910080780010979113L;

	@Column(name = "city", nullable = false)
	private String city;

	@ManyToOne(fetch = FetchType.LAZY)
	private Country country;

	public String getCity() {
		return city;
	}

	public void setCity(String pCity) {
		city = pCity;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country pCountry) {
		country = pCountry;
	}


}
