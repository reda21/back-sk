/**
 *
 */

package com.custom.backend.model;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;


/**
 * @author Redinho16
 *
 */
@Entity
@javax.persistence.Table(name = "T_RENTAL")
public class Rental extends CommonClass implements Serializable {


    /**
	 * 
	 */
	private static final long serialVersionUID = 6348912784675957951L;

	@Column(name = "rental_date", nullable = true)
    private LocalDate rental_date;

    @OneToOne
    @MapsId
    @JoinColumn(name = "id")
    private Inventory inventory;

    @Column(name = "return_date", nullable = true)
    private LocalDate return_date;

    @ManyToOne(fetch = FetchType.LAZY)
    private Custom customer;

    @ManyToOne(fetch = FetchType.LAZY)
    private Staff staff;


    /**
     * @return the rentalDate
     */
    public LocalDate getRentalDate() {
        return rental_date;
    }

    /**
     * @param pRentalDate the rentalDate to set
     */
    public void setRentalDate(LocalDate pRentalDate) {
        rental_date = pRentalDate;
    }

    /**
     * @return the inventoryId
     */

    public Inventory getInventory() {
        return inventory;
    }

    /**
     * @param pInventory the inventory to set
     */

    public void setInventory(Inventory pInventory) {
        inventory =
                pInventory;
    }


    /**
     * @return the returnDate
     */

    public LocalDate getReturnDate() {
        return return_date;
    }

    /**
     * @param pReturnDate the returnDate to set
     */

    public void setReturnDate(LocalDate pReturnDate) {
        return_date = pReturnDate;
    }
}
