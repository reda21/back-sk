/**
 * 
 */

package com.custom.backend.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;

/**
 * @author Redinho16
 *
 */

@javax.persistence.Entity
@javax.persistence.Table(name = "T_CATEGORY")
public class Category extends CommonClass implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -748849051570974321L;

	@Column(name = "name", nullable = false)
	private String name;

	@OneToMany(mappedBy = "category", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<Film> filmCategories = new HashSet<Film>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the filmCateogries
	 */
	public Set<Film> getFilmCateogries() {
		return filmCategories;
	}


	public void setFilmCateogries(Set<Film> pFilmCategories) {
		filmCategories = pFilmCategories;
	}

}
