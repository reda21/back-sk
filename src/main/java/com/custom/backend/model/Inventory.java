
/**
* 
*/
package com.custom.backend.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

/**
 * @author Redinho16
 *
 */

@Entity
@javax.persistence.Table(name = "T_INVENTORY")
public class Inventory extends CommonClass implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5153334449575699114L;

	@ManyToOne(fetch = FetchType.LAZY)
	private Film film;

	@ManyToOne(fetch = FetchType.LAZY)
	private Store store;

	public Film getFilm() {
		return film;
	}

	public void setFilm(Film film) {
		this.film = film;
	}


	public Store getStore() {
		return store;
	}

	public void setStore(Store store) {
		this.store = store;
	}


}
