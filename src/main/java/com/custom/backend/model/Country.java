/**
 * 
 */
package com.custom.backend.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;


/**
 * @author Redinho16
 *
 */
@Entity
@javax.persistence.Table(name="T_COUNTRY")
public class Country extends CommonClass implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9119407670723032062L;

	@Column(name = "country", nullable = false)
	private String name;

	@OneToMany(mappedBy = "country", cascade = CascadeType.ALL , fetch = FetchType.LAZY)
	private Set<City> cities = new HashSet<City>();


	/**
	 * @return the cities
	 */
	public Set<City> getCities() {
		return cities;
	}

	/**
	 * @param pCities the cities to set
	 */
	public void setCities(Set<City> pCities) {
		cities = pCities;
	}
}
