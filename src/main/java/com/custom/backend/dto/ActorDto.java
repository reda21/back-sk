
/**
 * 
 */
package com.custom.backend.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Redinho16
 *
 */
public class ActorDto extends CommonClassDto implements Serializable {
	
	private String firstName;

	private String lastName;

	private Set<FilmDto> films = new HashSet<>();


	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Set<FilmDto> getFilms() {
		return films;
	}

	public void setFilms(Set<FilmDto> films) {
		this.films = films;
	}

}


