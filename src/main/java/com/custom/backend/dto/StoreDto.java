/**
 * 
 */

package com.custom.backend.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Redinho16
 *
 */
public class StoreDto extends CommonClassDto implements Serializable {

	private StaffDto staff;

	private Set<CustomerDto> customers = new HashSet<>();

	private Set<InventoryDto> inventories = new HashSet<InventoryDto>();

	public Set<CustomerDto> getCustomers() {
		return customers;
	}

	public void setCustomers(Set<CustomerDto> customerDtos) {
		this.customers = customers;
	}

	public StaffDto getStaff() {
		return staff;
	}

	public void setStaff(StaffDto pStaff) {
		staff = pStaff;
	}

	/**
	 * @return the inventories
	 */

	public Set<InventoryDto> getInventories() {
		return inventories;
	}

	/**
	 * @param pInventories the inventories to set
	 */
	public void setInventories(Set<InventoryDto> pInventories) {
		inventories = pInventories;
	}

}
