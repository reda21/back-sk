
/**
* 
*/
package com.custom.backend.dto;

import java.io.Serializable;

/**
 * @author Redinho16
 *
 */

public class InventoryDto extends CommonClassDto implements Serializable {

	private FilmDto film;

	private StoreDto store;

	public FilmDto getFilm() {
		return film;
	}

	public void setFilm(FilmDto filmDto) {
		this.film = film;
	}


	public StoreDto getStore() {
		return store;
	}

	public void setStore(StoreDto store) {
		this.store = store;
	}


}
