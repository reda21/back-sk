/**
 * 
 */

package com.custom.backend.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Redinho16
 *
 */
public class CustomerDto extends CommonClassDto implements Serializable {

	private String firstName;

	private String lastName;

	private String email;

	private AddressDto addressDto;

	private boolean active;

	private Set<RentalDto> rentalDtos = new HashSet<RentalDto>();

	private Set<PaymentDto> paymentDtos = new HashSet<>();

	private StoreDto storeDto;

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param pFirstName the firstName to set
	 */
	public void setFirstName(String pFirstName) {
		firstName = pFirstName;
	}

	/**
	 * @return the lastName
	 */


	public String getLastName() {
		return lastName;
	}

	/**
	 * @param pLastName the lastName to set
	 */

	public void setLastName(String pLastName) {
		lastName = pLastName;
	}

	/**
	 * @return the email
	 */


	public String getEmail() {
		return email;
	}

	/**
	 * @param pEmail the email to set
	 */

	public void setEmail(String pEmail) {
		email = pEmail;
	}

	/**
	 * @return the adress
	 */

  	//@OneToOne
  	//@JoinColumn(name="address", nullable=false)
  	//public Address getAddress() { return address; }

	/**
	 * @param pAddress the address to set
	 */

	//public void setAddress(Address pAddress) {
	//	address = pAddress;
	//}

	/**
	 * @return the active
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * @param pActive the active to set
	 */

	public void setActive(boolean pActive) {
		active = pActive;
	}


	/**
	 * @return the rentals
	 */

	public Set<RentalDto> getRentals() {
		return rentalDtos;
	}

	/**
	 * @param pRentalDtos the rentals to set
	 */
	public void setRentals(Set<RentalDto> pRentalDtos) {
		rentalDtos = pRentalDtos;
	}


	public Set<PaymentDto> getPayments() {
		return paymentDtos;
	}

	public void setPayments(Set<PaymentDto> paymentDtos) {
		this.paymentDtos = paymentDtos;
	}

	public StoreDto getStore() {
		return storeDto;
	}

	public void setStore(StoreDto storeDto) {
		this.storeDto = storeDto;
	}

}
