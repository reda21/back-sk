package com.custom.backend.dto;

import javax.persistence.*;
import java.time.LocalDate;

@MappedSuperclass
public abstract class CommonClassDto {

    private Long id;

    private LocalDate lastUpdate;

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    private LocalDate createDate;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

}
