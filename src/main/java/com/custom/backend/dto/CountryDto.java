/**
 * 
 */
package com.custom.backend.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


/**
 * @author Redinho16
 *
 */
public class CountryDto extends CommonClassDto implements Serializable {

	private String name;

	private Set<CityDto> cities = new HashSet<CityDto>();


	/**
	 * @return the cities
	 */
	public Set<CityDto> getCities() {
		return cities;
	}

	/**
	 * @param pCities the cities to set
	 */
	public void setCities(Set<CityDto> pCities) {
		cities = pCities;
	}
}
