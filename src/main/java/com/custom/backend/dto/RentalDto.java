/**
 *
 */

package com.custom.backend.dto;

import java.io.Serializable;
import java.time.LocalDate;


/**
 * @author Redinho16
 *
 */
public class RentalDto extends CommonClassDto implements Serializable {

    private LocalDate rental_date;

    private InventoryDto inventory;

    private LocalDate return_date;

    private CustomerDto customer;

    private StaffDto staff;


    /**
     * @return the rentalDate
     */
    public LocalDate getRentalDate() {
        return rental_date;
    }

    /**
     * @param pRentalDate the rentalDate to set
     */
    public void setRentalDate(LocalDate pRentalDate) {
        rental_date = pRentalDate;
    }

    /**
     * @return the inventoryId
     */

    public InventoryDto getInventory() {
        return inventory;
    }

    /**
     * @param pInventory the inventory to set
     */

    public void setInventory(InventoryDto pInventory) {
        inventory = pInventory;
    }


    /**
     * @return the returnDate
     */

    public LocalDate getReturnDate() {
        return return_date;
    }

    /**
     * @param pReturnDate the returnDate to set
     */

    public void setReturnDate(LocalDate pReturnDate) {
        return_date = pReturnDate;
    }
}
