/**
 * 
 */

package com.custom.backend.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Redinho16
 *
 */
public class CategoryDto extends CommonClassDto implements Serializable {

	private String name;

	private Set<FilmDto> filmCategories = new HashSet<FilmDto>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the filmCateogries
	 */
	public Set<FilmDto> getFilmCategories() {
		return filmCategories;
	}


	public void setFilmCategories(Set<FilmDto> pFilmDtoCategories) {
		filmCategories = pFilmDtoCategories;
	}

}
