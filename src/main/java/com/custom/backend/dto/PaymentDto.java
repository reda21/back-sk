/**
 * 
 */

package com.custom.backend.dto;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * @author Redinho16
 *
 */
public class PaymentDto extends CommonClassDto implements Serializable {

	private CustomerDto customer;

	private StaffDto staff;

	private RentalDto rental;

	private Double amount;

	private LocalDate payment_date;

	public CustomerDto getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerDto customerDto) {
		this.customer = customerDto;
	}

	public StaffDto getStaff() {
		return staff;
	}

	public void setStaff(StaffDto staffDto) {
		this.staff = staffDto;
	}

	public RentalDto getRental() {
		return rental;
	}

	public void setRental(RentalDto rentalDto) {
		this.rental = rentalDto;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public LocalDate getPayment_date() {
		return payment_date;
	}

	public void setPayment_date(LocalDate payment_date) {
		this.payment_date = payment_date;
	}



}
