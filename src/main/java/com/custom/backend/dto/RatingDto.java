/**
 * 
 */
package com.custom.backend.dto;

import com.custom.backend.model.Rating;

/**
 * @author Redinho16
 *
 */
public enum RatingDto {

	G("G"), P("P"), PG13("PG-13"), R("R"), NC17("NC-17");

	private String value;

	private RatingDto(String value) {
	this.value = value;
	}

	public String getValue() {
		return value;
	}
}
