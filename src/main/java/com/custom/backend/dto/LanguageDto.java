/**
 * 
 */
package com.custom.backend.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Redinho16
 *
 */
public class LanguageDto extends CommonClassDto implements Serializable {

	private Set<FilmDto> films = new HashSet<FilmDto>();

	private String name;

	public Set<FilmDto> getFilms() {
		return films;
	}

	public void setFilms(Set<FilmDto> filmDtos) {
		this.films = films;
	}


	public String getName() {
		return name;
	}

	public void setName(String pName) {
		name = pName;
	}
}
