/**
 * 
 */
package com.custom.backend.dto;

import java.io.Serializable;


/**
 * @author Redinho16
 *
 */
public class CityDto extends CommonClassDto implements Serializable {

	private String city;

	private CountryDto country;

	public String getCity() {
		return city;
	}

	public void setCity(String pCity) {
		city = pCity;
	}

	public CountryDto getCountry() {
		return country;
	}

	public void setCountry(CountryDto pCountry) {
		country = pCountry;
	}


}
