package com.custom.backend.dto;

import com.custom.backend.model.Rating;

public class RatingSource {

    Rating value;
    public Rating getValue() {
        return value;
    }
    public void setValue(Rating value) {
        this.value = value;
    }
}
