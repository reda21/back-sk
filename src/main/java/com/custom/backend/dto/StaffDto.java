/**
 *
 */

package com.custom.backend.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Blob;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Redinho16
 *
 */
public class StaffDto extends CommonClassDto implements Serializable {

    private String first_name;

    private String last_name;

    private AddressDto address;

    private Blob picture;

    private String email;

    private StoreDto store;

    private int active;

    private String username;

    private String password;

    private Set<RentalDto> rentals = new HashSet<RentalDto>();

    private Set<PaymentDto> payments = new HashSet<PaymentDto>();


    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public AddressDto getAddress() {
        return address;
    }

    public void setAddress(AddressDto address) {
        this.address = address;
    }

    public Blob getPicture() {
        return picture;
    }

    public void setPicture(Blob picture) {
        this.picture = picture;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public StoreDto getStore() {
        return store;
    }

    public void setStore(StoreDto store) {
        this.store = store;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<RentalDto> getRentals() {
        return rentals;
    }

    public void setRentals(Set<RentalDto> rentals) {
        this.rentals = rentals;
    }

    public Set<PaymentDto> getPayments() {
        return payments;
    }

    public void setPayments(Set<PaymentDto> payments) {
        this.payments = payments;
    }
}
		 