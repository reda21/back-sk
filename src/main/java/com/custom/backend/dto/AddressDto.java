/**
 * 
 */
package com.custom.backend.dto;

import java.io.Serializable;

/**
 * @author Redinho16
 *
 */
public class AddressDto extends CommonClassDto implements Serializable {

	private String address1;

	private String address2;

	private String district;

	private CityDto city;

	private String postalCode;

	private String phoneNumber;

	private double location;

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public CityDto getCityDto() {
		return city;
	}

	public void setCityDto(CityDto city) {
		this.city = city;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}


	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public double getLocation() {
		return location;
	}

	public void setLocation(double location) {
		this.location = location;
	}

}
