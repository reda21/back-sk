/**
 * 
 */

package com.custom.backend.dto;

import java.io.Serializable;
import java.time.Year;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Redinho16
 *
 */
public class FilmDto extends CommonClassDto implements Serializable {

	private String title;

	private String description;

	private int release_year;

	private LanguageDto language;

	private int original_language_id;

	private int rental_duration = 3;

	private double rental_rate = 4.99;

	private int length;

	private double replacement_cost = 19.99;

	private String rating;

	private String special_features;

	private Set<ActorDto> actors = new HashSet<>();

	private CategoryDto category;

	private Set<InventoryDto> inventory = new HashSet<>();

	private byte[] image;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getRelease_year() {
		return release_year;
	}

	public void setRelease_year(int release_year) {
		this.release_year = release_year;
	}

	public LanguageDto getLanguage() {
		return language;
	}

	public void setLanguage(LanguageDto language) {
		this.language = language;
	}

	public int getOriginal_language_id() {
		return original_language_id;
	}

	public void setOriginal_language_id(int original_language_id) {
		this.original_language_id = original_language_id;
	}

	public int getRental_duration() {
		return rental_duration;
	}

	public void setRental_duration(int rental_duration) {
		this.rental_duration = rental_duration;
	}

	public double getRental_rate() {
		return rental_rate;
	}

	public void setRental_rate(double rental_rate) {
		this.rental_rate = rental_rate;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public double getReplacement_cost() {
		return replacement_cost;
	}

	public void setReplacement_cost(double replacement_cost) {
		this.replacement_cost = replacement_cost;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public String getSpecial_features() {
		return special_features;
	}

	public void setSpecial_features(String special_features) {
		this.special_features = special_features;
	}

	public Set<ActorDto> getActors() {
		return actors;
	}

	public void setActors(Set<ActorDto> actors) {
		this.actors = actors;
	}

	public CategoryDto getCategory() {
		return category;
	}

	public void setCategory(CategoryDto categoryDto) {
		this.category = categoryDto;
	}

	public Set<InventoryDto> getInventory() {
		return inventory;
	}

	public void setInventory(Set<InventoryDto> inventory) {
		this.inventory = inventory;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}




}
