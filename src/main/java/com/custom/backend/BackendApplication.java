package com.custom.backend;

import javax.persistence.EntityManagerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
//@org.springframework.boot.autoconfigure.EnableAutoConfiguration
//@EnableJpaRepositories(includeFilters = @Filter(value = {org.springframework.stereotype.Repository.class},
//type = FilterType.ANNOTATION))
public class BackendApplication {

	@Autowired
	private EntityManagerFactory entityManagerFactory;

	public static void main(String[] args) {
		SpringApplication.run(BackendApplication.class, args);
	}

}
